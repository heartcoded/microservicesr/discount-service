package com.dianabb.microservices.discountservice.controller;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dianabb.microservices.discountservice.config.DiscountConfiguration;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(description = "Discount Service main controller exposing one single rest API: calculate-discount")
public class DiscountController {
	final static Logger LOGGER = Logger.getLogger(DiscountController.class);
	@Autowired
	private Environment env;
	@Autowired
	private DiscountConfiguration discountConfiguration;
	

	
	
	@GetMapping("/calculate-discount/{productType}")
	@ApiOperation(value="Calculate discount based on current hour and product type.Method returns BigDecimal")
	@ApiResponses(value = {
	@ApiResponse(code= 200,message="Ok status" , response = BigDecimal.class) })
	public BigDecimal calculateDiscount(@PathVariable(value = "productType") String productType) {
		LOGGER.info("Port is "+env.getProperty("local.server.port"));
		BigDecimal totalDiscount = BigDecimal.ZERO;
		int hourOfTheDay = getHourOfTheDay();
		totalDiscount = getProductDiscountBasedOnHour(hourOfTheDay).add(getProductDiscountBasedOnType(productType));
		LOGGER.info(getProductDiscountBasedOnHour(hourOfTheDay) + ", " + getProductDiscountBasedOnType(productType));
		LOGGER.info("Total discount returned: " +totalDiscount);
		return totalDiscount;
	}

	private int getHourOfTheDay() {

		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	private BigDecimal getProductDiscountBasedOnHour(int hourOfTheDay) {

		if (hourOfTheDay >= 0 && hourOfTheDay <= 8) {
			return BigDecimal.valueOf(0.1);
		} else if (hourOfTheDay > 8 && hourOfTheDay <= 17) {
			return discountConfiguration.getDuringDayDiscount();
		} else if (hourOfTheDay > 17 && hourOfTheDay <= 20) {
			return BigDecimal.valueOf(0.3);
		} else
			return BigDecimal.ZERO;

	}

	private BigDecimal getProductDiscountBasedOnType(String type) {

		switch (type) {
		case "SHIRT":
			return BigDecimal.valueOf(0.05);
		case "SKIRT":
			return BigDecimal.valueOf(0.1);
		case "HAT":
			return BigDecimal.valueOf(0.15);
		default:
			return BigDecimal.ZERO;
		}

	}

}
