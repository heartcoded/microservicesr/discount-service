package com.dianabb.microservices.discountservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {

	/**
	 * 
	 * Wanted to expose only controllers from our controller package , nothing else
	 * @return
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
	            .apis(RequestHandlerSelectors.basePackage("com.dianabb.microservices.discountservice.controller"))
	            .paths(PathSelectors.any())
	            .build()
                .apiInfo(metaData());
	}
	
	 private ApiInfo metaData() {
	        return new ApiInfoBuilder()
	                .title("discount-service microservice documentation")
	                .description("Microservice which calculates discount based on current hour and product type")
	                .version("1.0.0")
	                .contact(new Contact("Diana Brad", null, "dianabbrad@gmail.com"))
	                .build();
	    }
}
