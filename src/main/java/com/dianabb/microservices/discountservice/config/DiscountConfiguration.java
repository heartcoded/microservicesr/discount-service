package com.dianabb.microservices.discountservice.config;

import java.math.BigDecimal;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("discount-service")
public class DiscountConfiguration {

	private BigDecimal duringDayDiscount;

	public BigDecimal getDuringDayDiscount() {
		return duringDayDiscount;
	}

	public void setDuringDayDiscount(BigDecimal duringDayDiscount) {
		this.duringDayDiscount = duringDayDiscount;
	}

}
